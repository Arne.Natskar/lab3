package INF102.lab3.sumList;

import java.util.List;

public class SumRecursive implements ISum {

    @Override
    public long sum(List<Long> list) {
        if (list.isEmpty()) {
            return 0;
        }
        Long value = list.get(0);
        list.remove(0);
        return value + sum(list);
    }


}