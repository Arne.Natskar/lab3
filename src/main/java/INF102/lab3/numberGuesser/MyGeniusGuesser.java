package INF102.lab3.numberGuesser;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.IntToDoubleFunction;

public class MyGeniusGuesser implements IGuesser {

    private Random rand = new Random();
    private int lowerbound;
    private int upperbound;
    private int pivot;
    List<Integer > UpperList = new ArrayList<>();
    List<Integer> LowerList  = new ArrayList<>();

    @Override
    public int findNumber(RandomNumber number) {
        lowerbound = number.getLowerbound();
        upperbound = number.getUpperbound();
        int numberGuess = upperbound / 2;
        return recursiveSearch(numberGuess, lowerbound, upperbound, number);
    }

    private int recursiveSearch(int numberGuess, int lowerbound, int upperbound, RandomNumber number) {
        if(number.guess(numberGuess) == 0){
            return numberGuess;
        }
        else if(number.guess(numberGuess) == 1){
            upperbound = numberGuess;
            numberGuess = numberGuess - ((upperbound-lowerbound)/2) ;
        }
        else{
            lowerbound = numberGuess;
            numberGuess = numberGuess + ((upperbound-lowerbound)/2);

        }
        return recursiveSearch(numberGuess, lowerbound, upperbound, number);
    }

}





