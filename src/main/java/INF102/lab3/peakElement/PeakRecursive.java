package INF102.lab3.peakElement;

import java.util.List;

public class PeakRecursive implements IPeak {

    @Override
    public int peakElement(List<Integer> numbers) {
        if (numbers.isEmpty())
            throw new IllegalArgumentException("Empty list");
        int n = numbers.size();
        int index = n;
        if (n == 1)
            return numbers.get(0);
        System.out.println(numbers);
        int prev = numbers.get(index - 3);
        int current = numbers.get(index - 2);
        System.out.println("første kjøring er: "+isPeak(prev, current, 0));
        if (isPeak(prev, current, 0))
            return numbers.get(index-1);
        index--;
        return helpFunction(numbers, n, index);
    }

    private Integer helpFunction(List<Integer> numbers, Integer n, Integer index) {
        //checkIndexOutOfBounds(n, index);
        if (index == 2) {
            System.out.println("index 3");
            int current = numbers.get(index - 2);
            int next = numbers.get(index-1);
            if(isPeak(0, current, next)){
                return numbers.get(0);
            }
        }
        int prev = numbers.get(index - 3);
        int current = numbers.get(index - 2);
        int next = numbers.get(index-1);
        System.out.println("Size:" + n);
        System.out.println("Index" + index);
        System.out.println(prev + ""+"current, " + current + ". Next:" + next);
        if (isPeak(prev, current, next))
            return current;
        return helpFunction(numbers, n, index - 1);
    }

    private void checkIndexOutOfBounds(Integer n, Integer index) {
        if(index < 0 || index > n){
            throw new IndexOutOfBoundsException();
        }
    }

    private boolean isPeak(int prev, int current, int next) {
        if(current > prev && current > next){
            System.out.println(":D");
        }
        return current > prev && current > next;
    }

}
